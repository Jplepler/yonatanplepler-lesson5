#include "Menu.h"
enum SHAPES
{
	CIRC = 0,
	ARW,
	TRI,
	REC
};
using namespace std;
Menu::Menu()
{
	Canvas _canvas;
}

Menu::~Menu()
{
}

Canvas Menu::getCanvas() const
{
	return _canvas;
}

void Menu::mainMenu() const
{
	cout << "Enter 0 to add a new shape.\n" <<
		"Enter 1 to modify or get information from a current shape.\n" <<
		"Enter 2 to delete all of the shapes.\n" <<
		"Enter 3 to exit.\n" << endl;
}

void Menu::addShapeMenu() const
{
	cout << "Enter 0 to add a circle.\n" <<
		"Enter 1 to add an arrow.\n" <<
		"Enter 2 to add a triangle.\n" << 
		"Enter 3 to add a rectangle." << endl;
}

void Menu::modMenu(const std::vector<Shape*>& shapes) const
{
	int i = 0;
	for (i = 0; i < shapes.size(); i++)
	{
		cout << "Enter " << i << " for " << shapes[i]->getName() << "(" << shapes[i]->getType() << ")" << endl;
	}
}


void Menu::shapeModMenu() const
{
	cout << "Enter 0 to move the shape" << endl <<
		"Enter 1 to get its details." << endl <<
		"Enter 2 to remove the shape." << endl;
}

//go through each shape and send it to the delete shape function
void Menu::clearAll(std::vector<Shape*>& shapes)
{
	while (shapes.size())
	{
		delShape(shapes, shapes.size() - 1);
	}
}

/*/////////////////////////////////
The next function create an instance of wanted object and put it into pointer to shape
In: Variables for constructing wanted object
Out: object as pointer to Shape object
*/
Shape* Menu::createCircle(double x, double y, double rad, string name)
{
	Point center(x, y);
	Shape* s = new Circle(center, rad, "Circle", name);
	return s;
}
Shape* Menu::createRectangle(double x, double y, double width, double length, string name)
{
	Point p(x, y);
	Shape* s = new myShapes::Rectangle(p, length, width, "Rectangle", name);
	return s;
}
Shape* Menu::createArrow(double x1, double y1, double x2, double y2, string name)
{
	Point p1(x1, y1);
	Point p2(x2, y2);
	Shape* s = new Arrow(p1, p2, "Arrow", name);
	return s;
}
Shape* Menu::createTriangle(double x1, double y1, double x2, double y2, double x3, double y3, string name)
{
	Point p1(x1, y1);
	Point p2(x2, y2);
	Point p3(x3, y3);
	Shape* s = new Triangle(p1, p2, p3, "Triangle", name);
	return s;
}
//////////////////////////////////////////

/*
This function adds the a shape to the vector of shapes and draws it
In: refrence to the Vector of shapes
*/
void Menu::addShape(std::vector<Shape*>& shapes)
{
	bool success = true;
	int choice = 0;
	double x1 = 0, x2 = 0, x3 = 0;
	double y1 = 0, y2 = 0, y3 = 0;
	double length = 0, width = 0;
	double rad = 0;
	Shape * shapePtr = nullptr;
	string name = "";

	do {
		cin >> choice;
	} while (choice < 0 || choice > REC );

	//get the correct data according to the chosen shape
	switch (choice)
	{
	case CIRC:
		cout << "Please enter X:" << endl;
		cin >> x1;
		cout << "Please enter Y:" << endl;
		cin >> y1;
		cout << "Please enter radius:" << endl;
		cin >> rad;
		cout << "Please enter the name of the shape:" << endl;
		cin >> name;
		shapePtr = createCircle(x1, y1, rad, name);
		break;

	case ARW:
		cout << "Please enter X: 1" << endl;
		cin >> x1;
		cout << "Please enter Y: 1" << endl;
		cin >> y1;
		cout << "Please enter X: 2" << endl;
		cin >> x2;
		cout << "Please enter Y: 2" << endl;
		cin >> y2;
		cout << "Please enter the name of the shape:" << endl;
		cin >> name;
		shapePtr = createArrow(x1, y1, x2, y2, name);
		break;

	case TRI:
		cout << "Please enter X: 1" << endl;
		cin >> x1;
		cout << "Please enter Y: 1" << endl;
		cin >> y1;
		cout << "Please enter X: 2" << endl;
		cin >> x2;
		cout << "Please enter Y: 2" << endl;
		cin >> y2;
		cout << "Please enter X: 3" << endl;
		cin >> x3;
		cout << "Please enter Y: 3" << endl;
		cin >> y3;
		cout << "Please enter the name of the shape:" << endl;
		cin >> name;
		
		if (x1 != x2 && x2 != x3 || y1 != y2 && y2 != y3)
		{
			shapePtr = createTriangle(x1, y1, x2, y2, x3, y3, name);
		}
		else
		{
			success = false;
			cout << "The points entered create a line." << endl;
			system("PAUSE");
		}

		break;

	case REC:
		cout << "Enter the X of the to left corner:" << endl;
		cin >> x1;
		cout << "Enter the Y of the to left corner:" << endl;
		cin >> y1;
		cout << "Please enter the length of the shape:" << endl;
		cin >> length;
		cout << "Please enter the width of the shape:" << endl;
		cin >> width;
		cout << "Please enter the name of the shape:" << endl;
		cin >> name;
		if (width > 0 && length > 0)//check if width and length are valid
		{
			shapePtr = createRectangle(x1, y1, length, width, name);
		}
		else
		{
			success = false;
		}
		
		break;

	default:
		break;
	}
	if(success)
	{
		shapes.push_back(shapePtr);
		shapePtr->draw(_canvas);
	}
}

/*
This function re draws all shapes from the vector
In: vector of shapes
*/
void Menu::reDraw(std::vector<Shape*>& shapes)
{
	int i = 0;
	for (i = 0; i < shapes.size(); i++)
	{
		shapes[i]->clearDraw(_canvas);
		shapes[i]->draw(_canvas);
	}
}

/*
This function deletes the chosen a shape from the vector
In: vector of shapes, index of chosen shape
*/
void Menu::delShape(std::vector<Shape*>& shapes, int index)
{
	int i = 0;
	std::vector<Shape*>::iterator t;//used as index in the vector for the erase function
	t = shapes.begin();
	//This loop increases t until it reaches the wanted index
	for (i = 0; i < index; i++) { t++; }

	shapes[index]->clearDraw(_canvas);
	delete shapes[index];
	shapes.erase(t);//Remove the shape from the vector
	
	//Redraw all the shapes so the deleted shape won't create uneccessery blank space
	this->reDraw(shapes); 

}

/*
This function moves the chosen shape with the inputted paramters (x, y)
In: Vector of shapes, index of chosen shape
*/
void Menu::movShape(std::vector<Shape*>& shapes, int index)
{
	double x = 0, y = 0;
	shapes[index]->clearDraw(_canvas);
	cout << "Please enter the X moving scale: ";
	cin >> x;
	cout << "Please enter the Y moving scale: ";
	cin >> y;
	Point p(x, y);
	//Set new cordinates for the shape
	shapes[index]->move(p);
	this->reDraw(shapes);//ReDraw all the shapes with the new cordinates
	shapes[index]->draw(_canvas);
}

