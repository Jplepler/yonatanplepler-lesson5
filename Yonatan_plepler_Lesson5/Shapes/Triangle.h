#pragma once
#include "Polygon.h"
#include <string>
#include <cmath>
#define SIDE1 0
#define SIDE2 1
#define SIDE3 2
#define HALF 0.5

class Triangle : public Polygon
{
public:
	Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name);
	virtual ~Triangle();
	
	// override functions if need (virtual + pure virtual)
	virtual void clearDraw(const Canvas& canvas);
	virtual void draw(const Canvas& canvas);
	virtual double getArea() const;
	virtual double getPerimeter() const;

};