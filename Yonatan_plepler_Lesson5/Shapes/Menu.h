#pragma once
#include "Circle.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "Canvas.h"

using namespace std;



class Menu
{
public:

	Menu();
	~Menu();

	// more functions..
	void mainMenu() const;
	void addShapeMenu() const;
	void modMenu(const std::vector<Shape*>& shapes) const;
	void shapeModMenu() const;
	void addShape(std::vector<Shape*>& shapes);
	void clearAll(std::vector<Shape*>& shapes);
	void reDraw(std::vector<Shape*>& shapes);
	void delShape(std::vector<Shape*>& shapes, int index);
	void movShape(std::vector<Shape*>& shapes, int index);
	Canvas getCanvas() const;




private: 
	Shape* createCircle(double x, double y, double rad, string name);//Circle
	Shape* createRectangle(double x, double y, double width, double length, string name);//rectangle
	Shape* createArrow(double x1, double y1, double x2, double y2, string name);//Arrow
	Shape* createTriangle(double x1, double y1, double x2, double y2, double x3, double y3, string name);//Triangle
	Canvas _canvas;
};

