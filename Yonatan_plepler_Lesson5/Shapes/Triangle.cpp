#include "Triangle.h"

//C'Tor
Triangle::Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name) : Polygon(type, name)
{
	this->_points.push_back(a);
	this->_points.push_back(b);
	this->_points.push_back(c);
}
//D'Tor
Triangle::~Triangle()
{

}
/*
Calclculate sum of the 3 distances between the 3 points
Out: the result of the calculating which is the Perimeter
*/
double Triangle::getPerimeter() const
{
	return _points[SIDE1].distance(_points[SIDE2]) + _points[SIDE2].distance(_points[SIDE3]) + _points[SIDE3].distance(_points[SIDE1]);
}
/*
Use Heron's formula for calculating area of a triangle
Out: area of this triangle
*/
double Triangle::getArea() const
{
	//Calculates the sides of the Triangle
	double a = _points[SIDE1].distance(_points[SIDE2]);
	double b = _points[SIDE2].distance(_points[SIDE3]);
	double c = _points[SIDE3].distance(_points[SIDE1]);
	double s = this->getPerimeter() * HALF;//Semi- Perimeter
	return sqrt(s * (s - a) * (s - b) * (s - c));//Heron's formula
}




void Triangle::draw(const Canvas& canvas)
{
	canvas.draw_triangle(_points[0], _points[1], _points[2]);
}

void Triangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_triangle(_points[0], _points[1], _points[2]);
}
