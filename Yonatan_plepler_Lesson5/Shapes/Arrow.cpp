#include "Arrow.h"
#include "Canvas.h"

//C'Tor
Arrow::Arrow(const Point& a, const Point& b, const std::string& type, const std::string& name) : Shape(name, type)
{
	std::vector<Point> _points;
	this->_points.push_back(a);
	this->_points.push_back(b);
	std::cout << _points.size();
}


Arrow::~Arrow()
{

}

//Arrow is a 2D shape thus, it has no area
double Arrow::getArea() const { return 0; }

double Arrow::getPerimeter() const
{
	return _points[0].distance(_points[1]);
}
void Arrow::move(const Point& other)
{
	_points[0] += other;
	_points[1] += other;
}

void Arrow::draw(const Canvas& canvas)
{
	canvas.draw_arrow(_points[0], _points[1]);
}
void Arrow::clearDraw(const Canvas& canvas)
{
	canvas.clear_arrow(_points[0], _points[1]);
}


