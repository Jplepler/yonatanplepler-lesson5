#include "Point.h"

//C'Tor
Point::Point(double x, double y)
{
	this->_x = x;
	this->_y = y;
}

//Copy C'Tor
Point::Point(const Point& other)
{
	_x = other.getX();
	_y = other.getY();
}

//D'Tor
Point::~Point()
{

}

//Overide operand + : return = this + other
Point Point::operator+(const Point& other) const
{
	Point newPoint(other.getX() + _x, other.getY() + _y);
	return newPoint;

}

//Override operand += : this = this + other
Point& Point::operator+=(const Point& other)
{
	_x += other.getX();
	_y += other.getY();
	return *this;
}

//Getters
double Point::getX() const
{
	return _x;
}
double Point::getY() const
{
	return _y;
}

//Calculate distance using formula of two points on a  straight line
double Point::distance(const Point& other) const
{
	return sqrt(pow(other.getY() - _y, SQUARE) + pow(other.getX() - _x, SQUARE));
}