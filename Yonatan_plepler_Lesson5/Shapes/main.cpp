
#include "Menu.h"
#define TWO 2
using namespace std;
enum OPTIONS
{
	NEW = 0,
	MOD,
	DEL,
	EXT
};

enum MODS
{
	MOV = 0,
	DET,
	REM
};

int main()
{
	int choice = 0;
	int shapeChoice = 0;
	int modChoice = 0;
	Menu m;//create a menu for the canvas
	std::vector<Shape*> shapeArray;

	
	do {
		m.mainMenu();
		cin >> choice;
		system("cls");
		switch (choice)
		{
			case NEW:
				m.addShapeMenu();
				m.addShape(shapeArray);
				break;

			case MOD:
				//If user added any shapes
				if (shapeArray.size() > 0)
				{
					//Get the wanted Shape
					do {//input + inputcheck
						m.modMenu(shapeArray);
						cin >> shapeChoice;
						system("cls");
	
					} while (shapeChoice >= shapeArray.size() || shapeChoice < 0);
	
					//Get the wanted modification
					do {//input + inputcheck
						m.shapeModMenu();
						cin >> modChoice;
						system("cls");
					} while (modChoice < 0 || modChoice > TWO);
					
					if (modChoice == MOV)
					{
						m.movShape(shapeArray, shapeChoice);
					}
					else if (modChoice == DET)
					{
						shapeArray[shapeChoice]->printDetails();
						system("PAUSE");
					}
					else if (modChoice == REM)
					{
						m.delShape(shapeArray, shapeChoice);
					}
				}
				break;
	
			case DEL:
				m.clearAll(shapeArray);
				break;
	
			case EXT:
				break;
	
			default:
				break;
		}
	
		system("cls");
	
	
	} while (choice != EXT);
	//Delete all shapes
	m.clearAll(shapeArray);
	return (0);
}