#include "Shape.h"

using namespace std;
Shape::Shape(const std::string& name, const std::string& type)
{
	_name = name;
	_type = type;
}
/*
This function prints the details of the shape
type  name       perimeter   area  
*/
void Shape::printDetails() const
{
	cout  << this->_type;
	cout << "  " <<this->_name;
	cout << "       " << this->getPerimeter();
	cout << "   " << this->getArea() << endl;
}
//Getters
string Shape::getType() const
{
	return _type;
}

string Shape::getName() const
{
	return _name;
}
