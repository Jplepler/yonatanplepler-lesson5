#include "Circle.h"

//C'Tor
Circle::Circle(const Point& center, double radius, const std::string& type, const std::string& name) : Shape(name, type), _center(center), _radius(radius)
{

}
Circle::~Circle()
{
	
}
//Getters
const Point& Circle::getCenter() const
{
	return _center;
}
double Circle::getRadius() const
{
	return _radius;
}
double Circle::getArea() const
{
	return PI * pow(_radius, SQUARE);
}
double Circle::getPerimeter() const
{
	return PI * _radius * SQUARE;
}
void Circle::move(const Point& other)
{
	_center += other;
}





void Circle::draw(const Canvas& canvas)
{
	canvas.draw_circle(getCenter(), getRadius());
}

void Circle::clearDraw(const Canvas& canvas)
{
	canvas.clear_circle(getCenter(), getRadius());
}


