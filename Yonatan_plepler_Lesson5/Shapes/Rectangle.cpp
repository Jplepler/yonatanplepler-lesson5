#include "Rectangle.h"

using namespace myShapes;

Rectangle::Rectangle(const Point& a, double length, double width, const std::string& type, const std::string& name) :
	Polygon(type, name)
{
	if (length > 0 && width > 0)
	{
		
		//Calculate the next 3 points
		Point b(a.getX(), a.getY() + length);
		Point c(a.getX() + width, a.getY());
		Point d(a.getX() + width, a.getY() + length);
		_points.push_back(a);//top left
		_points.push_back(b);//bottom left
		_points.push_back(c);//top right
		_points.push_back(d);//bottom right
	}

}
Rectangle::~Rectangle()
{
	
}
//Calculate area (Length * width)
double Rectangle::getArea() const
{
	double length = _points[SIDE1].distance(_points[SIDE2]);//length
	double width = _points[SIDE1].distance(_points[SIDE3]);//width
	return length * width;
}
//Calculate Perimeter (Width * 2 + Length * 2)
double Rectangle::getPerimeter() const
{
	double length = _points[SIDE1].distance(_points[SIDE2]);//length
	double width = _points[SIDE1].distance(_points[SIDE3]);//width
	return TWO * length  + TWO * width;
}

void Rectangle::draw(const Canvas& canvas)
{
	canvas.draw_rectangle(_points[0], _points[3]);
}

void Rectangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_rectangle(_points[0], _points[3]);
}


